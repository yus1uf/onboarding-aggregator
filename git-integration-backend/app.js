require("dotenv").config();
const express = require("express")
const userRouter = require("./src/routes/userRoutes")
const {connectToDb} = require("./src/utility/db")
const mongoose = require("mongoose")
const app = express();
const port = process.env.PORT;
const db = mongoose.connection;
app.use(express.json())
app.use(
    express.urlencoded({
        extended: true
    })
);

app.get("/", (req,res)=>{
    res.send("<h1>Welcome to git integration page</h1>")
});
app.use(userRouter)
app.listen(port, async()=>{
    await connectToDb()
    console.log("Server started running on ",port)
});