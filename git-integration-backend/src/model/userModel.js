const mongoose = require("mongoose")

const userSchema = mongoose.Schema({
        name: {
            type: String
        },
        username: {
            type: String
        },
        id: {
            type: Number
        },
        state: {
            type: String
        },
        created_at: {
            type: Date
        },
        email: {
            type: String
        }
})

const userModel = mongoose.model("gitlab_user",userSchema)

module.exports = userModel;