require("dotenv").config();
const DB = process.env.DB_URL;


const mongoose = require("mongoose");
const db = mongoose.connection;

const connectToDb = () => {
  mongoose.connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  db.on("error", console.error.bind(console, "connection error: "));
  db.on("open", console.error.bind(console, "DB connected: "));
  db.on("close", console.error.bind(console, "Connection closed"));
};

const closeConnection = () => {
  db.close();
};

module.exports = { connectToDb, closeConnection };
