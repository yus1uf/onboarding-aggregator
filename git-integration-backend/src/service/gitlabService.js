const {Gitlab} = require("@gitbeaker/node")

const gitlab = new Gitlab( {
    host: process.env.GIT_HOST,
    token: process.env.GIT_TOKEN
})

const createAccount = async (userData) =>{
    try {
        let data = await gitlab.Users.create(userData)
        return data
    } catch (error) {
        return error
    }
}

const blockAccount = async (userId) => {
    try {
        const details = await gitlab.Users.block(userId)
        return details;
    } catch (error) {
        return error
    }
}

module.exports = {createAccount,blockAccount}