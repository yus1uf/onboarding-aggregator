const userModel = require("../model/userModel")

const insertData = async(details)=>{
  try {
    const data = await new userModel({
        name: details.name,
        username: details.username,
        id: details.id,
        state: details.state,
        created_at: details.created_at,
        email: details.email
    }).save()
    return data
  } catch (error) {
      return error;
  }
}

const getData = async () =>{
    try {
        let data = await userModel.find().sort({state:1});
        return data;
    } catch (error) {
        return error;
    }
}

const updateStatus = async(mongoId) =>{
    try {
        const data = await userModel.updateOne(
            { _id: mongoId },
            {
              $set: { state: "blocked" },
            }
          );
          return data;
    } catch (error) {
        return error;
    }
}

module.exports = {insertData,updateStatus,getData}