const {customResponse} = require("../utility/helper")
const {createAccount,blockAccount} = require("../service/gitlabService")
const {insertData,updateStatus,getData} = require("../service/userService")


const getUser = async (req,res) => {
    let code,message
    try {
         code = 200;
        let data = await getData();
        const resData = customResponse({code,data})
       return res.status(code).send(resData)
    } catch (error) {
        code =500;
        message = "Internal Server Error"
        const resData = customResponse({code,message,err:error})
        return res.status(code).send(resData)
    }

}

const createUser = async (req,res) =>{
    let code, message;
    try {
        const userData = req.body;
        code = 200;
        let details = await createAccount(userData)
        const data = await insertData(details)
        const resData = customResponse({code,data})
       return res.status(code).send(resData)
    } catch (error) {
        code =500;
        message = "Unable to create user"
        const resData = customResponse({code,message,err:error})
        return res.status(code).send(resData)
    }
}

const blockUser = async (req,res) =>{
    let code, message;
    try {
        code = 200
        let userId = parseInt(req.params.id);
        const mongoId=req.body.mongoId;
        const details = await blockAccount(userId)
        const data = await updateStatus(mongoId)
        const resData = customResponse(code,data)
        return res.status(code).send(resData)
    } catch (error) {
        code =500;
        message = "Unable to create user"
        const resData = customResponse({code,message,err:error})
        return res.status(code).send(resData)
    }
}

module.exports = {getUser,
    createUser,
    blockUser
}