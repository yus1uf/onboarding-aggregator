const express = require("express")
const Router = express.Router();

const {getUser,
    createUser,
    blockUser,
  
} = require("../controller/userController")

Router.get("/user",getUser)
Router.post("/user",createUser)
Router.post("/users/:id/block",blockUser)


module.exports = Router