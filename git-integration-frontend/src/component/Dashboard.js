import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@mui/material/IconButton";
import Popup from "./Popup";
import "./style.css";
const Dashboard = (props) => {
  const [users, setUsers] = useState([]);
  const [open, setOpen] = useState(false);
  useEffect(async () => {
    let data = await fetch("/user", { method: "GET" });
    let result = await data.json();
    setUsers(result.data);
  }, [open]);

  const togglePopup = () => {
    setOpen(!open);
  };
  const deleteUser = async (param,param2) => {
    console.log(typeof param2)
    const dataToSend = {
      mongoId: param2
    }
    let data = await fetch(`/users/${param}/block`,{
      method: "POST",
      body: JSON.stringify(dataToSend),
      headers: { "Content-Type": "application/json" }
    })
    if (data.status === 200) {
      alert("User Blocked Successfully");
      let data = await fetch("/user", { method: "GET" });
      let result = await data.json();
      setUsers(result.data);
    } else {
      let err = await data.json();
      alert(err.error.description);
    }
    
  };
  return (
    <div className="main">
      <div className="button">
        <Button variant="contained" onClick={togglePopup}>
          New User
        </Button>
      </div>
      <h1>List Of Users</h1>
      <div className="table">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 350 }} aria-label="simple teble">
            <TableHead className="tableHead">
              <TableRow>
                <TableCell>
                  {" "}
                  <b>Name</b>
                </TableCell>
                <TableCell>
                  <b>Username</b>
                </TableCell>
                <TableCell>
                  <b>Email</b>
                </TableCell>
                <TableCell>
                  <b>Status</b>
                </TableCell>
                <TableCell>
                  <b>Action</b>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((row) => (
                <TableRow
                  key={row.name}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell>{row.username}</TableCell>
                  <TableCell>{row.email}</TableCell>
                  <TableCell>{row.state}</TableCell>
                  <TableCell>
                    <IconButton
                      aria-label="delete"
                      color="error"
                      disabled = {row.state==="blocked"}
                      onClick={() => deleteUser(row.id,row._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      {open && <Popup handleClose={togglePopup} />}
    </div>
  );
};

export default Dashboard;
