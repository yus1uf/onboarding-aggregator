import React from "react";
import {
  makeStyles,
  Container,
  Typography,
  TextField,
  Button,
} from "@material-ui/core";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";
import "./style.css";

const schema = yup.object().shape({
  name: yup.string().required().min(2).max(25),
  email: yup.string().required().email(),
  username: yup.string().required().min(3).max(25),
});

const useStyles = makeStyles((theme) => ({
  heading: {
    textAlign: "center",
    margin: theme.spacing(1, 0, 4),
  },
  submitButton: {
    marginTop: theme.spacing(4),
  },
}));

const Popup = (props) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const [newUser, setNewUser] = useState({
    name: "",
    email: "",
    username: "",
  });
  const { heading, submitButton } = useStyles();

  const onSubmit = async () => {
    const dataToSend = { ...newUser, reset_password: true };
    let details = await fetch("/user", {
      method: "POST",
      body: JSON.stringify(dataToSend),
      headers: { "Content-Type": "application/json" },
    });

    if (details.status === 200) {
      alert("User Created Successfully");
      props.handleClose();
    } else {
      let err = await details.json();
      alert(err.error.description);
    }
  };

  const handleInput = (e) => {
    const fieldName = e.target.name;
    const value = e.target.value;
    setNewUser({ ...newUser, [fieldName]: value });
  };

  return (
    <div className="popup-box">
      <div className="box">
        <span className="close-icon" onClick={props.handleClose}>
          x
        </span>
        <Container maxWidth="xs">
          <Typography className={heading} variant="h5">
            Create New User
          </Typography>
          <form onSubmit={handleSubmit(onSubmit)} noValidate>
            <TextField
              {...register("name")}
              onChange={handleInput}
              variant="outlined"
              margin="normal"
              label="Full Name"
              helperText={errors.name?.message}
              error={!!errors.name?.message}
              fullWidth
              required
            />
            <TextField
              {...register("email")}
              onChange={handleInput}
              variant="outlined"
              margin="normal"
              label="Email"
              helperText={errors.email?.message}
              error={!!errors.email?.message}
              fullWidth
              required
            />
            <TextField
              {...register("username")}
              onChange={handleInput}
              variant="outlined"
              margin="normal"
              label="Username"
              helperText={errors.username?.message}
              error={!!errors.username?.message}
              fullWidth
              required
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={submitButton}
            >
              Create User
            </Button>
          </form>
        </Container>
      </div>
    </div>
  );
};

export default Popup;
